package com.idfy;

import java.util.*;

import com.idfy.constants.Tasks;
import com.idfy.constants.Constants;

public class Utils {

    private String taskType;
    private String taskId;
    private String groupId;
    private Map<String, String> data;
    private Tasks tasks = new Tasks();

    Utils(String taskType, String taskId, Map<String, String> data, String groupId) {
        this.taskType = taskType;
        this.taskId = taskId;
        this.groupId = groupId;
        this.data = data;
    }

    Utils(String taskType, String taskId, Map<String, String> data) {
        this.taskType = taskType;
        this.taskId = taskId;
        this.data = data;
    }

    Utils() { }

    protected Map<String, String> validateResponseQueryArguments(Map<String, String> responseQueryArgs) throws IDfyErrors{
        if (responseQueryArgs.isEmpty()) {
            throw new IDfyErrors("Empty responseQueryArgs provided. Accepted key(s) are any of the following - \"request_id\", \"group_id\", \"task_id\".");
        }
        for (String key: responseQueryArgs.keySet()) {
            if (!Constants.getResponseQueryArgs().contains(key)) {
                throw new IDfyErrors("Invalid argument given to query the response - " + key.concat(". ") + "Accepted key(s) are any of the following - \"request_id\", \"group_id\", \"task_id\".");
            }
        }
        return responseQueryArgs;
    }

    protected Map<String, Object> validateRequestArguments(String apiVersion) throws IDfyErrors {
        if (this.taskType == null || this.taskType.isEmpty()) {
            throw new IDfyErrors("Empty/null task_type provided. Refer the doc for task_types - https://api-docs.idfy.com/v2/#task-types");
        } else if (!Constants.getV2AvailableTasks().get(apiVersion).contains(this.taskType)) {
            throw new IDfyErrors("Invalid task_type requested. Refer the doc for task_types - " +
                    "https://api-docs.idfy.com/v2/#task-types");
        }

        if (this.taskId == null || this.taskId.isEmpty()) {
            throw new IDfyErrors("Empty/null task_type provided. Refer the doc for task_id - https://api-docs.idfy.com/v2/#task-types");
        }

        if (this.data == null || this.data.isEmpty()) {
            throw new IDfyErrors("Empty/null data provided. Refer the doc for data - https://api-docs.idfy.com/v2/#task-types\"");
        }

        if (null != this.groupId && this.groupId.isEmpty()) {
            throw new IDfyErrors("Empty groupId provided. Refer the doc for group_id - https://api-docs.idfy.com/v2/#task-types\"");
        }

        this.validateRequestData(apiVersion);
        Map<String, Object> requestBody;
        requestBody = this.updateRequestBody();
        return requestBody;
    }

    private Map<String, Object> updateRequestBody() {
        final List<Map<String, Object>> tasksList = new ArrayList<>();
        Map<String, Object> tasksMap = new HashMap<>();
        tasksMap.put("type", this.taskType);
        tasksMap.put("task_id", this.taskId);
        tasksMap.put("data", this.data);
        if (null != this.groupId) {
            tasksMap.put("group_id", this.groupId);
        }

        tasksList.add(tasksMap);
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("tasks", tasksList);
        return requestBody;
    }

    private void validateRequestData(String apiVersion) throws IDfyErrors {
        Set<String> inputDataKeys = this.data.keySet();
        Map<String, ArrayList<String>> taskSchema;
        ArrayList<String> allTaskDataFields = new ArrayList<>();
        taskSchema = tasks.taskConfig.get(apiVersion).get("dataSchema").get(this.taskType);
        ArrayList<String> mandateTaskFields = taskSchema.get(Constants.getMandateFields());
        if (!mandateTaskFields.isEmpty()) {
            allTaskDataFields.addAll(mandateTaskFields);
            for (String k : mandateTaskFields) {
                if (!inputDataKeys.contains(k)) {
                    throw new IDfyErrors("Insufficient data-fields provided. " +
                            "Expected mandatory fields in data object are: " + mandateTaskFields);
                }

            }
        }
        ArrayList<String> optionalTaskFields = taskSchema.get(Constants.getOptionalFields());
        if (!optionalTaskFields.isEmpty()) {
            allTaskDataFields.addAll(optionalTaskFields);
        }
        ArrayList<String> anyTaskFields = taskSchema.get(Constants.getAnyFields());
        if (!anyTaskFields.isEmpty()) {
            allTaskDataFields.addAll(anyTaskFields);
            boolean anyFieldsFlag = false; // Flagging presence of any field
            for (String anyField : anyTaskFields) {
                if (inputDataKeys.contains(anyField)) {
                    anyFieldsFlag = true;
                    break;
                }
            }
            if (!anyFieldsFlag) { //Raising error if any_fields_flag is None
                throw new IDfyErrors(
                        "Insufficient data-fields provided. Expected at least one of these fields - " + anyTaskFields);
            }
        }
        // Checking if any of the input data_fields are not required or empty
        for (String field: inputDataKeys) {
            if (this.data.get(field).isEmpty()) {
                throw new IDfyErrors("Empty value provided for the key - " + field);
            }
            if (!allTaskDataFields.contains(field)) {
                throw new IDfyErrors("Unexpected data-field provided in data - " + field + " in " +
                        this.data.toString() + ". Acceptable fields are : " + allTaskDataFields);
            }
        }
    }
}
