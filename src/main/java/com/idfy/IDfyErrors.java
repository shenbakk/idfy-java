package com.idfy;

public class IDfyErrors extends Exception {

    public IDfyErrors(String message) {
        super(message);
    }

    public IDfyErrors(String message, Throwable cause) {
        super(message, cause);
    }

    public IDfyErrors(Throwable cause) {
        super(cause);
    }

    public IDfyErrors(String message, Throwable cause, boolean enableSuppression,
                             boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
